fun main(): Unit {

    val age: Byte = 25
    val myCharacter: Char = 'w'
    val s: Short = 100
    val myNumber: Int = 2000
    val l: Long = 20000L

    val pi: Float = 3.14F
    val d: Double = 9.23

    val b: Boolean = false

    val p = sayHello("Guliko")
    println(p)
    sayHello()

}

fun sayHello(name: String = ""): String {
    return "Hello $name!"
}